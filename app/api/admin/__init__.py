from flask import Blueprint
from flask_restful import Api

from app.exception import APIException
from app.api.admin.endpoints import login, get_me, get_qiniu_token, recharge, check_order, get_branch_orders, \
    modify_password, change_password, export_orders
from app.api.admin.resoures import AdminUserResource, AdminUsersResource, BannerResource, BannersResource, \
    BranchResource, BranchesResource, BrandResource, BrandsResource, CategoryResource, CategoriesResource, \
    GoodsResource, GoodsListResource, OrderResource, OrdersResource, UserResource, UsersResource, UserAddressResource, \
    UserAddressesResource, ExplainResource, ExplainListResource

bp_admin = Blueprint('admin', __name__, url_prefix='/api/admin')
api = Api(bp_admin, errors=APIException)

bp_admin.route('/auth/login', methods=['POST'])(login)
api.endpoints.add('login')

bp_admin.route('/auth/me', methods=['GET'])(get_me)
api.endpoints.add('get_me')

bp_admin.route('/qiniu/token', methods=['GET'])(get_qiniu_token)
api.endpoints.add('get_qiniu_token')

api.add_resource(AdminUserResource, '/admin_users/<int:user_id>')
api.add_resource(AdminUsersResource, '/admin_users')

api.add_resource(BannerResource, '/banners/<int:banner_id>')
api.add_resource(BannersResource, '/banners')

api.add_resource(BranchResource, '/branches/<int:branch_id>')
api.add_resource(BranchesResource, '/branches')

api.add_resource(BrandResource, '/brands/<int:brand_id>')
api.add_resource(BrandsResource, '/brands')

api.add_resource(CategoryResource, '/categories/<int:category_id>')
api.add_resource(CategoriesResource, '/categories')

api.add_resource(GoodsResource, '/goods/<int:goods_id>')
api.add_resource(GoodsListResource, '/goods')

# api.add_resource(ExplainResource, '/explain/<int:explain_id>')
# api.add_resource(ExplainListResource, '/explain')

# 导出订单
bp_admin.route('/orders/export', methods=['GET', 'OPTIONS'])(export_orders)
api.endpoints.add('export_orders')

api.add_resource(OrderResource, '/orders/<int:order_id>')
api.add_resource(OrdersResource, '/orders')

api.add_resource(UserResource, '/users/<int:user_id>')
api.add_resource(UsersResource, '/users')

api.add_resource(UserAddressResource, '/user_addresses/<int:address_id>')
api.add_resource(UserAddressesResource, '/user_addresses')

# 修改密码
bp_admin.route('/modify_password', methods=['PUT', 'OPTIONS'])(modify_password)
api.endpoints.add('modify_password')

# 未登录修改密码
bp_admin.route('/change_password', methods=['PUT', 'OPTIONS'])(change_password)
api.endpoints.add('change_password')

# 分店充娃娃
bp_admin.route('/recharge', methods=['POST'])(recharge)
api.endpoints.add('recharge')
# 分店兑换娃娃（确认订单）
bp_admin.route('/orders/<int:order_id>/check', methods=['PUT'])(check_order)
api.endpoints.add('check_order')
# 分店查询订单
bp_admin.route('/branch_orders', methods=['GET'])(get_branch_orders)
api.endpoints.add('get_branch_orders')

