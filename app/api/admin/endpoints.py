from sqlalchemy import func

from app.api.admin.fields import admin_user_fields, order_fields, order_list_fields
from app.api.admin.parsers import admin_login_args, recharge_args, check_order_args, get_branch_orders_args, \
    modify_admin_user_password_args, pagination_parser, change_admin_user_password_args, get_param_parser, \
    order_filter_parser, date_filter_parser
from app.exception import APIException, USER_NOT_EXISTS, ORDER_NOT_EXISTS, NOT_ALLOWED_EXCHANGE, \
    ORDER_ALREADY_PROCESSED, DATABASE_ERROR, USER_IS_LOCKED, USERNAME_OR_PASSWORD_WRONG
from app.models import AdminUser, User, UserRecord, Order, Goods
from app.extensions import auth, roles_accepted, db
from flask_restful import marshal
from flask import jsonify, g, current_app
from qiniu import Auth as QiniuAuth


def login():
    args = admin_login_args.parse_args()
    admin_user = AdminUser.query.filter_by(username=args['username']).first()
    if admin_user and admin_user.locked is True:
        # return jsonify({'coed': -1, 'message': '账号已被冻结'})
        raise APIException(USER_IS_LOCKED)
    if admin_user and admin_user.verify_password(args['password']):
        data = marshal(admin_user, admin_user_fields)
        return jsonify({
            'code': 0,
            'token': admin_user.generate_auth_token(),
            'userInfo': data
        }), 200
    else:
        # return jsonify({'code': -1, 'message': '用户名或密码错误'}), 200
        raise APIException(USERNAME_OR_PASSWORD_WRONG)


def logout():
    pass


def change_password():
    """ 未登录修改密码 """
    args = change_admin_user_password_args.parse_args()
    admin_user = AdminUser.query.filter_by(username=args['username']).first()
    if admin_user is None:
        # return jsonify({'coed': -1, 'message': '用户不存在'})
        raise APIException(USER_NOT_EXISTS)
    if admin_user and admin_user.locked is True:
        # return jsonify({'coed': -1, 'message': '账号已被冻结'})
        raise APIException(USER_IS_LOCKED)
    old_password = args.get('old_password')
    if not admin_user.verify_password(old_password):
        raise APIException(USERNAME_OR_PASSWORD_WRONG)
    password = args.pop('password')
    if password:
        admin_user.set_password(password)
    admin_user.update(True,)
    return jsonify({
        'code': 0,
        'message': '修改密码成功'
    }), 200


@auth.login_required
def get_me():
    return jsonify({
        'code': 0,
        'data': marshal(g.user, admin_user_fields)
    }), 200


@auth.login_required
def modify_password():
    """ 修改密码 """
    user = g.user
    # if user.id != user_id:
    #     raise APIException('没有权限')
    args = modify_admin_user_password_args.parse_args()
    old_password = args.get('old_password')
    if not user.verify_password(old_password):
        raise APIException('原密码错误')
    password = args.pop('password')
    if password:
        user.set_password(password)
    user.update(True,)
    return jsonify({
        'code': 0,
        'message': '修改密码成功'
    }), 200


@auth.login_required
def get_qiniu_token():
    access_key = current_app.config['QINIU_ACCESS_KEY']
    secret_key = current_app.config['QINIU_SECRET_KEY']

    # 构建鉴权对象
    q = QiniuAuth(access_key, secret_key)
    # 要上传的空间
    bucket_name = 'img-zhuawawa'
    # 上传到七牛后保存的文件名
    key = None
    # 生成上传 Token，可以指定过期时间等
    # 上传策略示例
    # https://developer.qiniu.com/kodo/manual/1206/put-policy
    policy = {
        # 'callbackUrl':'https://requestb.in/1c7q2d31',
        # 'callbackBody':'filename=$(fname)&filesize=$(fsize)'
        # 'persistentOps':'imageView2/1/w/200/h/200'
    }
    # 3600为token过期时间，秒为单位。3600等于一小时
    token = q.upload_token(bucket_name, key, 3600, policy)
    data = {
       'bucketHost': 'https://static.bianfuyule.com/',
       'key': key,
       'token': token,
    }
    return jsonify({
        'code': 0,
        'data': data
    }), 200


@auth.login_required
@roles_accepted('manager')
def recharge():
    args = recharge_args.parse_args()
    admin_user = g.user
    user_id = args.get('user_id')
    doll = args.get('doll')
    # user = User.query.get(user_id)
    user = User.query.filter_by(uid=user_id).first()
    if not user:
        raise APIException(USER_NOT_EXISTS)
    user.doll += doll
    if user.branch_id == 0:
        user.branch_id = admin_user.branch_id
    user.update(False, )
    record_row = {
        'user_id': user.id,
        'type': 1,
        'doll': doll,
        'operate_id': admin_user.id
    }
    UserRecord.create(False, **record_row)
    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        raise APIException(DATABASE_ERROR)
    return jsonify({
        'code': 0,
        'message': '存娃娃成功',
        'data': {}
    }), 200


@auth.login_required
@roles_accepted('manager')
def check_order(order_id):
    order = Order.query.get(order_id)
    if not order:
        raise APIException(ORDER_NOT_EXISTS)
    if order.status != 0:
        raise APIException(ORDER_ALREADY_PROCESSED)
    admin_user = g.user
    if order.branch_id != admin_user.branch_id:
        raise APIException(NOT_ALLOWED_EXCHANGE)
    user = User.query.get(order.user_id)
    args = check_order_args.parse_args()
    check = args.get('check')
    if check:
        order.status = 3
    else:
        order.status = 4
        # 拒绝 娃娃加回去
        user.doll += order.amount
        user.update(False, )
		# 娃娃兑换个数减回去
        goods = Goods.query.get(order.goods_id)
        goods.exchange_num -= 1
        goods.update(False, )
        # 插入用户娃娃变更记录
        record_row = {
            'user_id': user.id,
            'type': 3,
            'doll': order.amount,
            'operate_id': admin_user.id
        }
        UserRecord.create(False, **record_row)
    order.update(False,)

    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        raise APIException(DATABASE_ERROR)

    return jsonify({
        'code': 0,
        'message': '成功',
        'data': {}
    }), 200


@auth.login_required
@roles_accepted('manager')
def get_branch_orders():
    args = get_branch_orders_args.parse_args()
    user_id = args.get('user_id')
    status = args.get('status')
    admin_user = g.user
    query = Order.query.filter_by(branch_id=admin_user.branch_id).filter_by(exchange_type=0)
    if user_id:
        query = query.filter_by(user_id=user_id)
    if status == 0:
        query = query.filter_by(status=status)
    elif status == 1:
        query = query.filter(Order.status > 0)
    query = query.order_by(Order.id.desc())

    page_args = pagination_parser.parse_args(req=args)
    page = page_args.get('page')
    per_page = page_args.get('per_page')

    _orders = query.paginate(page, per_page=per_page, error_out=False)

    orders = {
        'data': marshal(_orders.items, order_list_fields),
        "pageSize": _orders.per_page,
        "pageNo": _orders.page,
        "totalPage": _orders.pages,
        "totalCount": _orders.total
    }

    return jsonify({
        'code': 0,
        'message': '成功',
        'data': orders
    }), 200


@auth.login_required
@roles_accepted('admin')
def export_orders():
    args = get_param_parser.parse_args()

    query = Order.query

    _filter = args.get('filter')
    if _filter:
        filter_args = order_filter_parser.parse_args(req=args)
        for k, v in filter_args.items():
            if v is not None:
                query = query.filter(getattr(Order, k) == v)

        date_filter_args = date_filter_parser.parse_args(req=args)
        start_date = date_filter_args.get('start_date')
        end_date = date_filter_args.get('end_date')
        if start_date:
            if end_date is None:
                end_date = start_date
            query = query.filter((func.date(Order.order_time)).between(start_date, end_date))

    orders = query.all()

    return jsonify({
        'code': 0,
        'message': '成功',
        'data': marshal(orders, order_list_fields)
    }), 200
