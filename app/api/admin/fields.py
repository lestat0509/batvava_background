from flask_restful import fields

from app.utils import SimpleDateField

admin_user_fields = {
    'id': fields.Integer,
    'username': fields.String,
    'real_name': fields.String,
    'phone_number': fields.String,
    'last_login_at': fields.String,
    'role': fields.String
}

banner_fields = {
    'id': fields.Integer,
    'category': fields.String,
    'position': fields.Integer,
    'image_url': fields.String,
    'json_data': fields.String,
    'start_time': SimpleDateField,
    'end_time': SimpleDateField,
    'weights': fields.Integer,
    'deleted': fields.Boolean
}

branch_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'manager': fields.String,
    'mobile': fields.String,
    'address': fields.String,
    'admin': fields.List(fields.Nested({
        'id': fields.Integer,
        'username': fields.String,
        'real_name': fields.String,
        'phone_number': fields.String,
    }))
}

brand_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String
}

category_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'pic_url': fields.String,
    'sort': fields.Integer
}

goods_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'sku': fields.String,
    'brand_id': fields.Integer,
    'brand': fields.Nested({
        'id': fields.Integer,
        'name': fields.String
    }),
    'category_id': fields.Integer,
    'category': fields.Nested({
        'id': fields.Integer,
        'name': fields.String
    }),
    'detail': fields.String,
    'weights': fields.Integer,
    'is_listing': fields.Boolean,
    'recommended': fields.Boolean,
    'is_online': fields.Boolean,
    'pic_url': fields.String,
    'exchange_price': fields.Integer,
    'stock': fields.Integer,
    'exchange_num': fields.Integer,
    'deleted': fields.Boolean,
    'created': SimpleDateField,
    'images': fields.List(fields.Nested(
        {
            'id': fields.Integer,
            'url': fields.String,
            'width': fields.Integer,
            'height': fields.Integer
        }
    ))
}

order_list_fields = {
    'id': fields.Integer,
    'order_no': fields.String,
    'goods_name': fields.String(attribute='goods.name'),
    'user_id': fields.Integer,
    'nickname': fields.String(attribute='user.nickname'),
    'branch_name': fields.String(attribute="branch.name"),
    'order_time': SimpleDateField,
    'exchange_type': fields.Integer,
    'status': fields.Integer,
    'amount': fields.Integer,
    'address': fields.Nested({
        'id': fields.Integer,
        'name': fields.String,
        'phone': fields.String,
        'address': fields.String,
    }),
    'pic_url': fields.String(attribute='goods.pic_url'),
    'courier_company': fields.String,
    'courier_no': fields.String
}

order_fields = {
    'id': fields.Integer,
    'order_no': fields.String,
    'goods_name': fields.String(attribute='goods.name'),
    'user_id': fields.Integer,
    'nickname': fields.String(attribute='user.nickname'),
    'branch_name': fields.String(attribute="branch.name"),
    'order_time': SimpleDateField,
    'exchange_type': fields.Integer,
    'status': fields.Integer,
    'amount': fields.Integer,
    'address': fields.Nested({
        'id': fields.Integer,
        'name': fields.String,
        'phone': fields.String,
        'address': fields.String,
        'province': fields.String,
        'city': fields.String,
        'county': fields.String
    }),
    'remark': fields.String,
    'pic_url': fields.String(attribute='goods.pic_url'),
    'courier_company': fields.String,
    'courier_no': fields.String
}

user_fields = {
    'id': fields.Integer,
    'uid': fields.String,
    'nickname': fields.String,
    'avatar': fields.String,
    'branch': fields.Nested(branch_fields),
    'doll': fields.Integer,
    'status': fields.Boolean,
    'gender': fields.Integer,
    'created': SimpleDateField,
}


explain_fields = {
    'id': fields.Integer,
    'image_url': fields.String,
    'json_data': fields.String,
}
