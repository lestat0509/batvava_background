import json

from flask_restful import reqparse

admin_login_args = reqparse.RequestParser(bundle_errors=True)
admin_login_args.add_argument('username', type=str, required=True)
admin_login_args.add_argument('password', type=str, required=True, default='123456')

get_param_parser = reqparse.RequestParser()
get_param_parser.add_argument('pagination', type=json.loads)
get_param_parser.add_argument('filter', type=json.loads)
get_param_parser.add_argument('sort', type=json.loads)

# 分页
pagination_parser = reqparse.RequestParser()
pagination_parser.add_argument('page', type=int, location=("pagination",))
pagination_parser.add_argument('per_page', type=int, location=("pagination",))

# 排序
sort_parser = reqparse.RequestParser()
sort_parser.add_argument('field', type=str, required=True, location=("sort",))
sort_parser.add_argument('reverse', type=bool, required=True, location=("sort",))

# 搜索
filter_parser = reqparse.RequestParser()
filter_parser.add_argument('keyword', type=str, location=("filter",))

# 按时间过滤
date_filter_parser = reqparse.RequestParser()
date_filter_parser.add_argument('start_date', type=str, location=("filter",))
date_filter_parser.add_argument('end_date', type=str, location=("filter",))

# 用户搜索
user_filter_parser = filter_parser.copy()
user_filter_parser.add_argument('uid', type=str, location=("filter",))

# 搜索
admin_user_filter_parser = filter_parser.copy()
admin_user_filter_parser.add_argument('username', type=str, location=("filter",))
admin_user_filter_parser.add_argument('real_name', type=str, location=("filter",))
admin_user_filter_parser.add_argument('phone_number', type=str, location=("filter",))

# 添加用户
add_admin_user_args = reqparse.RequestParser()
add_admin_user_args.add_argument('username', type=str, required=True, help='用户名是必需的')
add_admin_user_args.add_argument('real_name', type=str)
add_admin_user_args.add_argument('phone_number', type=str)
add_admin_user_args.add_argument('branch_id', type=int)
add_admin_user_args.add_argument('role', type=str, required=True, help='角色是必需的')
add_admin_user_args.add_argument('password', type=str, required=True)

# 更新用户
update_admin_user_args = reqparse.RequestParser()
# update_user_args.add_argument('username', type=str)
update_admin_user_args.add_argument('real_name', type=str)
update_admin_user_args.add_argument('phone_number', type=str)
update_admin_user_args.add_argument('password', type=str)
# update_admin_user_args.add_argument('role', type=str)

# 修改密码
modify_admin_user_password_args = reqparse.RequestParser()
modify_admin_user_password_args.add_argument('old_password', required=True, type=str)
modify_admin_user_password_args.add_argument('password', required=True, type=str)

# 未登录修改密码
change_admin_user_password_args = reqparse.RequestParser()
change_admin_user_password_args.add_argument('username', required=True, type=str)
change_admin_user_password_args.add_argument('old_password', required=True, type=str)
change_admin_user_password_args.add_argument('password', required=True, type=str)

# 添加轮播图
add_banner_args = reqparse.RequestParser()
add_banner_args.add_argument('category', type=str)
add_banner_args.add_argument('position', type=int)
add_banner_args.add_argument('image_url', type=str, required=True, help='轮播图图片地址是必需的')
add_banner_args.add_argument('json_data', type=str)
add_banner_args.add_argument('weights', type=int)

# 更新轮播图
update_banner_args = reqparse.RequestParser()
update_banner_args.add_argument('image_url', type=str)
update_banner_args.add_argument('weights', type=int)

# 添加分店
add_branch_args = reqparse.RequestParser()
add_branch_args.add_argument('name', type=str, required=True)
add_branch_args.add_argument('manager', type=str, required=True)
add_branch_args.add_argument('mobile', type=str, required=True)
add_branch_args.add_argument('address', type=str, required=True)

# 更新分店
update_branch_args = reqparse.RequestParser()
update_branch_args.add_argument('name', type=str)
update_branch_args.add_argument('manager', type=str)
update_branch_args.add_argument('mobile', type=str)
update_branch_args.add_argument('address', type=str)
update_branch_args.add_argument('enabled', type=bool)

# 搜索分店
branch_filter_parser = filter_parser.copy()
branch_filter_parser.add_argument('name', type=str, location=("filter",))
branch_filter_parser.add_argument('manager', type=str, location=("filter",))
branch_filter_parser.add_argument('mobile', type=str, location=("filter",))

# 添加品牌
add_brand_args = reqparse.RequestParser()
add_brand_args.add_argument('name', type=str, required=True)
add_brand_args.add_argument('description', type=str)

# 更新品牌
update_brand_args = reqparse.RequestParser()
update_brand_args.add_argument('name', type=str)
update_brand_args.add_argument('description', type=str)


# 添加分类
add_category_args = reqparse.RequestParser()
add_category_args.add_argument('name', type=str, required=True)
add_category_args.add_argument('pic_url', type=str, required=True)
add_category_args.add_argument('sort', type=int)

# 更新分类
update_category_args = reqparse.RequestParser()
update_category_args.add_argument('name', type=str)
update_category_args.add_argument('sort', type=int)
update_category_args.add_argument('pic_url', type=str)

# 添加订单 线下兑换
add_order_args = reqparse.RequestParser()
add_order_args.add_argument('goods_id', type=str, required=True)
add_order_args.add_argument('user_id', type=int)
add_order_args.add_argument('branch_id', type=int)
add_order_args.add_argument('amount', type=int)
add_order_args.add_argument('remark', type=str)

# 更新订单
update_order_args = reqparse.RequestParser()
update_order_args.add_argument('status', type=int)
update_order_args.add_argument('courier_company', type=str)
update_order_args.add_argument('courier_no', type=str)

# 搜索订单
order_filter_parser = filter_parser.copy()
order_filter_parser.add_argument('order_no', type=str, location=("filter",))
order_filter_parser.add_argument('status', type=int, location=("filter",))
order_filter_parser.add_argument('exchange_type', type=int, location=("filter",))

# 添加商品
add_goods_args = reqparse.RequestParser()
add_goods_args.add_argument('name', type=str, required=True)
add_goods_args.add_argument('sku', type=str, required=True)
add_goods_args.add_argument('brand_id', type=int, required=True)
add_goods_args.add_argument('category_id', type=int, required=True)
add_goods_args.add_argument('detail', type=str, required=True)
add_goods_args.add_argument('weights', type=int)
add_goods_args.add_argument('is_listing', type=bool)
add_goods_args.add_argument('recommended', type=bool)
add_goods_args.add_argument('is_online', type=bool)
add_goods_args.add_argument('pic_url', type=str, required=True)
add_goods_args.add_argument('exchange_price', type=int, required=True)
add_goods_args.add_argument('stock', type=str)
add_goods_args.add_argument('exchange_num', type=int)
add_goods_args.add_argument('images', type=dict, action='append', default=[])

# 更新商品
update_goods_args = reqparse.RequestParser()
update_goods_args.add_argument('name', type=str)
update_goods_args.add_argument('sku', type=str)
update_goods_args.add_argument('brand_id', type=int)
update_goods_args.add_argument('category_id', type=int)
update_goods_args.add_argument('detail', type=str)
update_goods_args.add_argument('weights', type=int)
update_goods_args.add_argument('is_listing', type=bool)
update_goods_args.add_argument('recommended', type=bool)
update_goods_args.add_argument('is_online', type=bool)
update_goods_args.add_argument('pic_url', type=str)
update_goods_args.add_argument('exchange_price', type=int)
update_goods_args.add_argument('stock', type=str)
update_goods_args.add_argument('exchange_num', type=int)
update_goods_args.add_argument('images', type=dict, action='append')

# 搜索商品
goods_filter_parser = filter_parser.copy()
goods_filter_parser.add_argument('name', type=str, location=("filter",))
goods_filter_parser.add_argument('is_listing', type=int, location=("filter",))
goods_filter_parser.add_argument('category_id', type=int, location=("filter",))
goods_filter_parser.add_argument('recommended', type=int, location=("filter",))

# 充值娃娃
recharge_args = reqparse.RequestParser()
recharge_args.add_argument('user_id', type=str, required=True)
recharge_args.add_argument('doll', type=int, required=True)

# 确认订单(门店兑换娃娃)
check_order_args = reqparse.RequestParser()
check_order_args.add_argument('check', type=bool, required=True)

get_branch_orders_args = reqparse.RequestParser()
get_branch_orders_args.add_argument('pagination', type=json.loads)
get_branch_orders_args.add_argument('user_id', type=int)
get_branch_orders_args.add_argument('status', type=int)


# 添加兑换说明
add_explain_args = reqparse.RequestParser()
add_explain_args.add_argument('category', type=str)
add_explain_args.add_argument('position', type=int)
add_explain_args.add_argument('image_url', type=str, required=True, help='兑换说明图片地址是必需的')
add_explain_args.add_argument('json_data', type=str)