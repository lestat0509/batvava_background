from flask import Blueprint
from flask_restful import Api

from app.exception import APIException
from app.api.wxapp.endpoints import login, get_me, get_category_list, get_sponsor_records, get_doll_records, \
    get_ranking, get_latest_orders, get_latest_orders_by_id, sponsor_doll, get_my_ranking
from app.api.wxapp.resoures import BannersResource, GoodsResource, GoodsListResource, OrderResource, OrderListResource, \
    BranchesResource, ExplainResource

bp_wxapp = Blueprint('wxapp', __name__, url_prefix='/api/wxapp')
api = Api(bp_wxapp, errors=APIException)

# 登录
bp_wxapp.route('/auth/login', methods=['POST'])(login)
api.endpoints.add('login')
# 获取当前登录用户信息
bp_wxapp.route('/auth/me', methods=['GET'])(get_me)
api.endpoints.add('get_me')
# 获取banner列表
api.add_resource(BannersResource, '/banners')

# 获取娃娃列表 搜索娃娃 获取分类下的娃娃列表
api.add_resource(GoodsListResource, '/goods')
# 娃娃详情
api.add_resource(GoodsResource, '/goods/<int:goods_id>')

# 获取分店
api.add_resource(BranchesResource, '/branches')

# 获取兑换说明
api.add_resource(ExplainResource, '/explain')

# 获取分类
bp_wxapp.route('/categories', methods=['GET'])(get_category_list)
api.endpoints.add('get_category_list')


# 最新动态(最新兑换记录)
bp_wxapp.route('/orders/latest', methods=['GET'])(get_latest_orders)
api.endpoints.add('get_latest_orders')
# 某一娃娃的最新动态(最新兑换记录)
bp_wxapp.route('/orders/latest/<int:goods_id>', methods=['GET'])(get_latest_orders_by_id)
api.endpoints.add('get_latest_orders_by_id')

# 订单详情
# 兑换娃娃post
api.add_resource(OrderResource, '/orders/<int:order_id>')
# 订单列表（兑换记录）
api.add_resource(OrderListResource, '/orders')

# 获取个人排行 type 0:娃娃榜 1:兑换榜
bp_wxapp.route('/ranking/me', methods=['GET'])(get_my_ranking)
api.endpoints.add('get_my_ranking')

# 排行榜type 0:娃娃榜 1:兑换榜
bp_wxapp.route('/ranking', methods=['GET'])(get_ranking)
api.endpoints.add('get_ranking')

# 赞助

# 赞助记录
bp_wxapp.route('/sponsor_records', methods=['GET'])(get_sponsor_records)
api.endpoints.add('get_sponsor_records')

# 娃娃使用明细
bp_wxapp.route('/doll_records', methods=['GET'])(get_doll_records)
api.endpoints.add('get_doll_records')

# 赞助娃娃
bp_wxapp.route('/sponsor', methods=['POST'])(sponsor_doll)
api.endpoints.add('sponsor_doll')
