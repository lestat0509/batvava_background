from flask_restful import fields

from app.utils import SimpleDateField

banner_fields = {
    'id': fields.Integer,
    'image_url': fields.String,
    'json_data': fields.String,
}

branch_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'manager': fields.String,
    'mobile': fields.String,
    'address': fields.String
}

category_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'pic_url': fields.String,
    'sort': fields.Integer,
}

goods_list_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'weights': fields.Integer,
    'is_listing': fields.Boolean,
    'recommended': fields.Boolean,
    'is_online': fields.Boolean,
    'pic_url': fields.String,
    'exchange_price': fields.Integer,
    'exchange_num': fields.Integer,
}

goods_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'sku': fields.String,
    'brand_id': fields.Integer,
    'brand': fields.Nested({
        'id': fields.Integer,
        'name': fields.String
    }),
    'category_id': fields.Integer,
    'category': fields.Nested({
        'id': fields.Integer,
        'name': fields.String
    }),
    'detail': fields.String,
    'weights': fields.Integer,
    'is_listing': fields.Boolean,
    'recommended': fields.Boolean,
    'is_online': fields.Boolean,
    'pic_url': fields.String,
    'exchange_price': fields.Integer,
    'stock': fields.Integer,
    'exchange_num': fields.Integer,
    'deleted': fields.Boolean,
    'created': SimpleDateField,
    'images': fields.List(fields.Nested(
        {
            'id': fields.Integer,
            'url': fields.String,
            'width': fields.Integer,
            'height': fields.Integer
        }
    ))
}

user_fields = {
    'id': fields.Integer,
    'uid': fields.String,
    'nickname': fields.String,
    'avatar': fields.String,
    # 'branch': fields.Nested(branch_fields),
    'branch_name': fields.String(attribute="branch.name"),
    'doll': fields.Integer,
    'status': fields.Boolean,
    'gender': fields.Integer,
    'created': SimpleDateField,
}


order_list_fields = {
    'id': fields.Integer,
    'order_no': fields.String,
    'goods_name': fields.String(attribute='goods.name'),
    'amount': fields.Integer,
    'order_time': SimpleDateField,
    'exchange_type': fields.Integer,
    'status': fields.Integer,
    'avatar': fields.String(attribute="user.avatar"),
    'pic_url': fields.String(attribute='goods.pic_url')
}


order_fields = {
    'id': fields.Integer,
    'order_no': fields.String,
    'goods_name': fields.String(attribute='goods.name'),
    'user_id': fields.Integer,
    'branch_name': fields.String(attribute="branch.name"),
    'order_time': SimpleDateField,
    'exchange_type': fields.Integer,
    'status': fields.Integer,
    'amount': fields.Integer,
    'address': fields.Nested({
        'id': fields.Integer,
        'name': fields.String,
        'phone': fields.String,
        'address': fields.String,
        'province': fields.String,
        'city': fields.String,
        'county': fields.String
    }),
    'remark': fields.String,
    'pic_url': fields.String(attribute='goods.pic_url'),
    'courier_company': fields.String,
    'courier_no': fields.String
}


lastest_order_fields = {
    'id': fields.Integer,
    'nickname': fields.String(attribute='user.nickname'),
    'avatar': fields.String(attribute='user.avatar'),
    'amount': fields.Integer,
    'goods_name': fields.String(attribute='goods.name'),
    'order_time': SimpleDateField,
}


rank_fields = {
    'user_id': fields.Integer,
    'nickname': fields.String,
    'avatar': fields.String,
    'doll': fields.Integer,
    'rank': fields.Integer
}

sponsor_records_fields = {
    'id': fields.Integer,
    'from_id': fields.Integer,
    'from_name': fields.String,
    'to_id': fields.Integer,
    'to_name': fields.String,
    'doll': fields.Integer,
    'created': SimpleDateField
}

doll_records_fields = {
    'id': fields.Integer,
    'user_id': fields.Integer,
    'type': fields.Integer,
    'doll': fields.Integer,
    'created': SimpleDateField
}

explain_fields = {
    'id': fields.Integer,
    'image_url': fields.String,
    'json_data': fields.String,
}