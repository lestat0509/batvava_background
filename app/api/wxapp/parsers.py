import json

from flask_restful import reqparse


get_param_parser = reqparse.RequestParser()
get_param_parser.add_argument('pagination', type=json.loads)
get_param_parser.add_argument('filter', type=json.loads)
get_param_parser.add_argument('sort', type=json.loads)

# 分页
pagination_parser = reqparse.RequestParser()
pagination_parser.add_argument('page', type=int, location=("pagination",))
pagination_parser.add_argument('per_page', type=int, location=("pagination",))

# 排序
sort_parser = reqparse.RequestParser()
sort_parser.add_argument('field', type=str, required=True, location=("sort",))
sort_parser.add_argument('reverse', type=bool, required=True, location=("sort",))

# 搜索
filter_parser = reqparse.RequestParser()
filter_parser.add_argument('name', type=str, location=("filter",))
filter_parser.add_argument('category_id', type=int, location=("filter",))
filter_parser.add_argument('recommended', type=int, location=("filter",))

# 搜索分店
branch_filter_parser = filter_parser.copy()
branch_filter_parser.add_argument('keyword', type=str, location=("filter",))

# 获取用户信息
get_user_args = reqparse.RequestParser(bundle_errors=True)
get_user_args.add_argument('code', type=str, required=True)
get_user_args.add_argument('encryptedData', type=str, required=True)
get_user_args.add_argument('iv', type=str, required=True)

# 微信小程序登录
wxapp_login_args = reqparse.RequestParser(bundle_errors=True)
wxapp_login_args.add_argument('code', type=str, required=True)
wxapp_login_args.add_argument('encryptedData', type=str, required=True)
wxapp_login_args.add_argument('iv', type=str, required=True)

# 排行榜
rank_type_args = reqparse.RequestParser(bundle_errors=True)
rank_type_args.add_argument('rank_type', type=int)
rank_type_args.add_argument('type', type=int)
rank_type_args.add_argument('top', type=int, default=10)

# 兑换娃娃 创建订单
add_order_args = reqparse.RequestParser(bundle_errors=True)
add_order_args.add_argument('goods_id', type=int, required=True)
add_order_args.add_argument('exchange_type', type=int, required=True)
add_order_args.add_argument('branch_id', type=int)
add_order_args.add_argument('address', type=dict)

# 赞助娃娃 创建订单
sponsor_args = reqparse.RequestParser(bundle_errors=True)
sponsor_args.add_argument('to_uid', type=int, required=True)
sponsor_args.add_argument('doll', type=int, required=True)
