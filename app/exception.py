""" 自定义错误 """
BANNER_NOT_EXISTS = -10001
BRAND_NOT_EXISTS = -10002
BRANCH_NOT_EXISTS = -10003
CATEGORY_NOT_EXISTS = -10004
GOODS_NOT_EXISTS = -10005
ORDER_NOT_EXISTS = -10006
USER_NOT_EXISTS = -10007  # 用户不存在
GOODS_NOT_LISTING = -10008
GOODS_NOT_ONLINE = -10009
ADDRESS_IS_REQUIRED = -10010
BRANCH_IS_REQUIRED = -10011
DOLL_NOT_ENOUGH = -10012
NOT_ALLOWED_EXCHANGE = -10013
ORDER_ALREADY_PROCESSED = -10014
USER_IS_LOCKED = -10015
USERNAME_OR_PASSWORD_WRONG = -10016
BRAND_NOT_NULL = -10017
CATEGORY_NOT_NULL = -10018
USERNAME_OR_PHONE_IS_EXISTS = -10019

DATABASE_ERROR = -30001
SERVER_ERROR = -30002
ERR_MSG = {
    BANNER_NOT_EXISTS: 'Banner不存在',
    BRAND_NOT_EXISTS: '品牌不存在',
    BRANCH_NOT_EXISTS: '该分店不存在',
    CATEGORY_NOT_EXISTS: '该分类不存在',
    ORDER_NOT_EXISTS: '订单不存在',
    GOODS_NOT_EXISTS: '商品不存在',
    USER_NOT_EXISTS: '用户不存在',
    GOODS_NOT_LISTING: '该商品已下架',
    GOODS_NOT_ONLINE: '该商品不允许线上兑换',
    ADDRESS_IS_REQUIRED: '线上兑换需要填写地址信息',
    BRANCH_IS_REQUIRED: '线下兑换必须选择门店',
    DOLL_NOT_ENOUGH: '娃娃不够',
    NOT_ALLOWED_EXCHANGE: '不是本店订单，不能兑换',
    ORDER_ALREADY_PROCESSED: '订单已经处理了',
    USER_IS_LOCKED: '账号已被冻结',
    USERNAME_OR_PASSWORD_WRONG: '用户名或密码错误',
    BRAND_NOT_NULL: '该品牌下还有商品，不能删除',
    CATEGORY_NOT_NULL: '该分类下还有商品，不能删除',
    USERNAME_OR_PHONE_IS_EXISTS: '用户名或手机号已被注册',
    DATABASE_ERROR: '数据库错误',
    SERVER_ERROR: '服务器错误'
}


class APIException(Exception):
    # 默认的返回码
    status_code = 200

    # 自己定义了一个 return_code，作为更细颗粒度的错误代码
    def __init__(self, code, status_code=None, payload=None):
        Exception.__init__(self)
        self.code = code
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    # 构造要返回的错误代码和错误信息的 dict
    def to_dict(self):
        rv = dict(self.payload or ())

        # 增加 dict key: return code
        rv['code'] = self.code

        # 增加 dict key: message, 具体内容由常量定义文件中通过 code 转化而来
        rv['message'] = ERR_MSG[self.code]

        return rv
