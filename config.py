import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_RECYCLE = 3
    SECRET_KEY = os.environ.get('SECRET_KEY')
    WX_APPID = os.environ.get('WX_APPID')
    WX_SECRET = os.environ.get('WX_SECRET')
    QINIU_ACCESS_KEY = os.environ.get('QINIU_ACCESS_KEY')
    QINIU_SECRET_KEY = os.environ.get('QINIU_SECRET_KEY')
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
